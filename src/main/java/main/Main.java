package main;

import dao.SetupDao;
import model.*;
import util.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        //loome skeemi
        try {
            new Main().run();
        } finally {
            JpaUtil.closeFactory();
        }
    }

    private void run() {
        new SetupDao().createSchema();

        insertUnit("Tehnika 27");
        insertUnit("Uus 1");

        System.out.println(findAllUnits());

//        User user = findUserByEmail("i@e.ee");
//        user.setEmail("i2@e.ee");
//        saveUser(user);

//        System.out.println(findAllUsers());
//        System.out.println();
    }

    private User findUserByEmail(String email) {
        EntityManager em = null;

        try {
            em = JpaUtil.getFactory().createEntityManager();

            TypedQuery<User> query = em.createQuery(
                    "select u from User u where u.email = :email", User.class);

            query.setParameter("email", email);
            return query.getSingleResult();


        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    private List<User> findAllUsers() {
        EntityManager em = null;

        try {
            em = JpaUtil.getFactory().createEntityManager();

            TypedQuery<User> query = em.createQuery("select u from User u", User.class);

            return query.getResultList();


        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    private List<Unit> findAllUnits() {
        EntityManager em = null;

        try {
            em = JpaUtil.getFactory().createEntityManager();

            TypedQuery<Unit> query = em.createQuery("select u from Unit u", Unit.class);

            return query.getResultList();


        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    private void saveUser(User user) {
        EntityManager em = null;

        try {
            em = JpaUtil.getFactory().createEntityManager();

            em.getTransaction().begin();

            em.merge(user);

            em.getTransaction().commit();


        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    private void insertUnit(String address) {
        EntityManager em = null;

        try {
            em = JpaUtil.getFactory().createEntityManager();

            em.getTransaction().begin();

            Unit unit = new Unit();
            unit.setAddress(address);

            Type type = new Type("Apartment");
            unit.setType(type);

            em.persist(unit);


            em.getTransaction().commit();


        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

}
