package model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class User {

    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "seq1", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    private Long id;
    private String email;
//    private String phone;
//    private String name;
//    private String lastname;

}
